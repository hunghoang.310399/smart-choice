package com.smartchoice.gatewaysmartchoice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class GatewaySmartchoiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(GatewaySmartchoiceApplication.class, args);
    }

}
