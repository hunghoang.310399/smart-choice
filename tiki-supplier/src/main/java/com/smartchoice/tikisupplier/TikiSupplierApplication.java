package com.smartchoice.tikisupplier;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient

public class TikiSupplierApplication {

    public static void main(String[] args) {
        SpringApplication.run(TikiSupplierApplication.class, args);
    }

}
