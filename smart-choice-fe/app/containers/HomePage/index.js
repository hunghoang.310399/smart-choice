/* eslint-disable react/prop-types */
/* eslint-disable indent */
/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 *
 */

import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import banner from '../../images/banner2.png';
import Playlists from '../../components/Playlists';

const useStyles = makeStyles({
  root: {
    '& > *': {
      fontFamily: ['Montserrat , sans-serif'],
    },
    '& a': { textDecoration: 'none' },
    '& h2': { marginLeft: '9%' },
  },

  link: {
    color: 'white',
  },
});

function HomePage(props) {
  const classes = useStyles();
  const query = decodeURIComponent(
    props.location.search.replace('?query=', ''),
  );

  return (
    <div className={classes.root}>
      <img className="banner" src={banner} alt="banner" />
      <Playlists query={query}  />
    </div>
  );
}
export default HomePage;
