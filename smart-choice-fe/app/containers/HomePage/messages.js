/*
 * HomePage Messages
 *
 * This contains all the text for the HomePage container.
 */
import { defineMessages } from 'react-intl';

export const scope = 'app.containers.HomePage';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the HomePage container!',
  },
  recommendation: {
    id: `${scope}.recommendation`,
    defaultMessage: 'Gợi ý cho bạn',
  },
  top_view: {
    id: `${scope}.top_view`,
    defaultMessage: 'Top view',
  },
  beauty: {
    id: `${scope}.beauty`,
    defaultMessage: 'Làm đẹp',
  },
  fnb: {
    id: `${scope}.fnb`,
    defaultMessage: 'Ăn uống gì?',
  },
  travel: {
    id: `${scope}.travel`,
    defaultMessage: 'Du lịch',
  },
  reviewer: {
    id: `${scope}.reviewer`,
    defaultMessage: 'Top REVIEWERS',
  },
  liking: {
    id: `${scope}.liking`,
    defaultMessage: 'Bạn vừa thích những gì nào?',
  },
});
