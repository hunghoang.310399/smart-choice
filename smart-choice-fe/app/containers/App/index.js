/**
 *
 * App.js
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 *
 */

import React, { memo, useEffect } from 'react';
import { Switch } from 'react-router-dom';
import HomePage from 'containers/HomePage/';
import './index.css';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { HomeTemplate } from '../../Templates/HomeTemplate/HomeTemplate';
import {
  fetchCategoriesError,
  fetchCategoriesSuccess,
} from '../../redux/actions/coreActions';
import Footer from '../../components/Footer';
function mapDispatchToProps(dispatch) {
  return {
    fetchCategories: () => {
      fetch('https://www.doranote.com/api/categories')
        .then(res => res.json())
        .then(res => {
          if (res.error) {
            throw res.error;
          }
          dispatch(fetchCategoriesSuccess(res));
          return res.Categories;
        })
        .catch(error => {
          dispatch(fetchCategoriesError(error));
        });
    },
  };
}

function App(props) {
  useEffect(() => {
    // eslint-disable-next-line react/prop-types
    props.fetchCategories();
  }, []);
  return (
    <div>
      <Switch>
        <HomeTemplate exact path="/" component={HomePage} />
        <Footer />
      </Switch>
    </div>
  );
}
const withConnect = connect(
  null,
  mapDispatchToProps,
);
export default compose(
  withConnect,
  memo,
)(App);
