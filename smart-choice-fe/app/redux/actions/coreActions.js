const {
  FETCH_CATEGORIES_PENDING,
  FETCH_CATEGORIES_SUCCESS,
  FETCH_CATEGORIES_ERROR,
  HANDLE_COLLAPSED,
  HANDLE_COLLAPSED_MOBILE,
  HANDLE_COLLAPSED_OVERLAY,
  SELECTED_PROFILE_PAGE,
} = require('../types/coreTypes');

export function selectedProfilePage(selected) {
  return {
    type: SELECTED_PROFILE_PAGE,
    payload: selected,
  };
}
export function fetchCategoriesPending() {
  return {
    type: FETCH_CATEGORIES_PENDING,
  };
}

export function fetchCategoriesSuccess(categories) {
  return {
    type: FETCH_CATEGORIES_SUCCESS,
    payload: categories,
  };
}

export function fetchCategoriesError(error) {
  return {
    type: FETCH_CATEGORIES_ERROR,
    error,
  };
}
export function handleCollapse(bool) {
  return {
    type: HANDLE_COLLAPSED,
    payload: bool,
  };
}
export function handleCollapseOverlay(bool) {
  return {
    type: HANDLE_COLLAPSED_OVERLAY,
    payload: bool,
  };
}
export function handleCollapseMobile(bool) {
  return {
    type: HANDLE_COLLAPSED_MOBILE,
    payload: bool,
  };
}
