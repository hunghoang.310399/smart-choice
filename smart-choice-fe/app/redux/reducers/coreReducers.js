/* eslint-disable no-unused-vars */
import produce from 'immer';
import {
  FETCH_CATEGORIES_ERROR,
  FETCH_CATEGORIES_PENDING,
  FETCH_CATEGORIES_SUCCESS,
  HANDLE_COLLAPSED,
  HANDLE_COLLAPSED_MOBILE,
  HANDLE_COLLAPSED_OVERLAY,
  SELECTED_PROFILE_PAGE,
} from '../types/coreTypes';

// The initial state of the App
const initialState = {
  pending: false,
  categories: [],
  error: null,
  collapse: true,
  collapseOverlay: false,
  collapseMobile: false,
  selectedProfile: null,
};

/* eslint-disable default-case, no-param-reassign */
const coreReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case FETCH_CATEGORIES_PENDING:
        return {
          ...state,
          pending: true,
        };
      case FETCH_CATEGORIES_SUCCESS:
        return {
          ...state,
          pending: false,
          categories: action.payload,
        };
      case FETCH_CATEGORIES_ERROR:
        return {
          ...state,
          pending: false,
          error: action.error,
        };
      case HANDLE_COLLAPSED: {
        return { ...state, collapse: action.payload };
      }
      case HANDLE_COLLAPSED_OVERLAY: {
        return { ...state, collapseOverlay: action.payload };
      }
      case HANDLE_COLLAPSED_MOBILE: {
        return { ...state, collapseMobile: action.payload };
      }
      case SELECTED_PROFILE_PAGE: {
        return { ...state, selectedProfile: action.payload };
      }
      default:
        return state;
    }
  });

export default coreReducer;
export const getCategories = state => state.categories;
export const getCategoriesPending = state => state.pending;
export const getCategoriesError = state => state.error;
export const selectedProfilePage = state => state.selectedProfile;
