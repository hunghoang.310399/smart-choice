/* eslint-disable react/prop-types */
import React, { Fragment } from 'react';
import { Route } from 'react-router-dom';
import Header from '../../components/Header';

export const HomeTemplate = props => (
  <Route
    path={props.path}
    {...props.exact}
    render={propsComponent => (
      <Fragment>
        <Header component={props.component} {...propsComponent} />
      </Fragment>
    )}
  />
);
