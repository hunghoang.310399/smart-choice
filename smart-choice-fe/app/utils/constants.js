export const RESTART_ON_REMOUNT = '@@saga-injector/restart-on-remount';
export const DAEMON = '@@saga-injector/daemon';
export const ONCE_TILL_UNMOUNT = '@@saga-injector/once-till-unmount';

export const REACTION_STATUS = { LIKE: 'LIKE', NONE: '', DISLIKE: 'DISLIKE' };
export const LANGUAGE = { VN: 'vn', EN: 'en' };
