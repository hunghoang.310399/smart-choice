/* eslint-disable react/prop-types */
/* eslint-disable no-unused-vars */
import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Grid, IconButton, Typography } from '@material-ui/core';
import FilterListIcon from '@material-ui/icons/FilterList';
import Faker from 'faker';
import Playlist from '../Playlist';

const useStyles = makeStyles(theme => ({
  root: {
    '& a': { textDecoration: 'none' },
  },
  header: {
    display: 'flex',
    justifyContent: 'space-between',
    '& > *': {
      margin: theme.spacing(1),
    },
  },
  headerLeft: { display: 'flex', justifyContent: 'space-between' },
  headerRight: { display: 'flex', justifyContent: 'space-between' },
  svgIcon: {
    width: 32,
    height: 32,
  },
  title: { fontWeight: '700', margin: theme.spacing(1) },
}));
export async function getStaticPaths({ params }) {
  // const postData = getPostData(params.id)
  return {
    paths: [], // indicates that no page needs be created at build time
    fallback: 'blocking', // indicates the type of fallback
  };
}

export default function Playlists(props) {
  const classes = useStyles();
  const createPlaylist = () => ({
    title: Faker.name.title(),
    thumbnail: Faker.random.image(),
    numberOfVideos: Faker.random.number(100),
  });
  const [products, setProducts] = useState();
  React.useEffect(() => {
    async function fetchVideos() {
      const formData = new FormData();
      // eslint-disable-next-line react/prop-types
      formData.append('query', props.query);
      const response = await fetch(
        `http://localhost:8760/rest/products/search?search=${props.query}`,
        {
          method: 'GET',
        },
      );
      const data = await response.json();

      const newArr = data;
      // while (data.length) newArr.push(data.splice(0, 4));
      setProducts(newArr);
    }
    fetchVideos();
  }, [props.query]);

  const createPlaylists = (numUsers = 5) =>
    Array.from({ length: numUsers }, createPlaylist);
  const fakePlaylists = createPlaylists(20);
  const [playlists] = React.useState(fakePlaylists);

  return (
    <div className={classes.root}>
      <div className={classes.header}>
        <div className={classes.headerLeft} />
        <div className={classes.headerRight}>
          <Typography variant="h4" gutterBottom className={classes.title}>
            Fillter
          </Typography>
          <IconButton aria-label="filter">
            <FilterListIcon fontSize="large" />
          </IconButton>
        </div>
      </div>
      <Grid
        container
        spacing={3}
        style={{
          width: '100%',
        }}
      >
        {products
          ? products.map((item, i) => (
              <Playlist key={i.toString()} playlist={item} />
            ))
          : undefined}

        {/* {playlists.map((playlist, index) => (
          <Playlist key={index.toString()} playlist={playlist} />
        ))} */}
      </Grid>
    </div>
  );
}
