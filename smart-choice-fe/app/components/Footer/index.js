import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import { NavLink } from 'react-router-dom';
import nature from '../../images/footerImage.png';
import play from '../../images/play.png';
import facebook from '../../images/facebook.svg';
import youtube from '../../images/youtube.svg';
import twitter from '../../images/twitter.svg';
import zalo from '../../images/zalo.svg';

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flex: 1,
    justifyContent: 'space-around',
    alignItems: 'center',
    '& > *': {
      margin: theme.spacing(4),
    },
    [theme.breakpoints.down('sm')]: {
      '& > *': {
        margin: theme.spacing(1),
        fontSize: 12,
      },
    },
  },
  spaceEven: {
    display: 'flex',
    justifyContent: 'space-evenly',
    '& > *': {
      margin: theme.spacing(1),
    },
  },
  svgIcon: {
    width: 32,
    height: 32,
    [theme.breakpoints.down('sm')]: {
      width: 16,
      height: 16,
    },
  },
  rightSide: {
    justifyContent: 'space-around',
    [theme.breakpoints.down('sm')]: {
      width: '85%',
      '& > *': {
        fontSize: 12,
      },
    },
  },
  imgNature: {
    [theme.breakpoints.down('sm')]: {
      width: '65%',
    },
  },
  imgPlay: {
    [theme.breakpoints.down('sm')]: {
      width: '20%',
    },
  },
  copyright: {
    display: 'flex',
    flex: 1,
    justifyContent: 'flex-start',
    '& > *': {
      margin: theme.spacing(1),
    },
  },
  privacyLink: { color: 'white' },
}));

export default function Footer() {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <div>
        <img className={classes.imgPlay} src={play} alt="play" />
        <img className={classes.imgNature} src={nature} alt="nature" />
        <div>
          <NavLink className={classes.privacyLink} to="/privacy">
            <Typography variant="subtitle1" gutterBottom>
            Privacy policy
            </Typography>
          </NavLink>
          <NavLink className={classes.privacyLink} to="/tos">
            <Typography variant="subtitle1" gutterBottom>
              Rule
            </Typography>
          </NavLink>
        </div>
        <Typography variant="subtitle1" gutterBottom>
          Copyright © 2020-2021 smartchoice
        </Typography>
      </div>
      <div className={classes.rightSide}>
        <Typography variant="h6" gutterBottom>
          Connect with our:
        </Typography>
        <div className={classes.spaceEven}>
          <a href="https://www.facebook.com/smartchoice.co" target="_blank">
            <img className={classes.svgIcon} src={facebook} alt="facebook" />
          </a>
          <a
            href="https://www.youtube.com/channel/UCTpnKxXPnMlaj3tCBca-Gmw"
            target="_blank"
          >
            <img className={classes.svgIcon} src={youtube} alt="youtube" />
          </a>
          <a href="https://twitter.com/dora_noted" target="_blank">
            <img className={classes.svgIcon} src={twitter} alt="twitter" />
          </a>
          <img className={classes.svgIcon} src={zalo} alt="zalo" />
        </div>
        <Typography>Email: hi@smartchoice.com</Typography>
        <Typography>Hotline: 0933449633</Typography>
      </div>
    </div>
  );
}
