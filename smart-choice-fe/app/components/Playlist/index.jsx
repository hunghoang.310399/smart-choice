import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Collapse from '@material-ui/core/Collapse';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import { red } from '@material-ui/core/colors';
import FavoriteIcon from '@material-ui/icons/Favorite';
import ShareIcon from '@material-ui/icons/Share';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import CardActionArea from '@material-ui/core/CardActionArea';
import Button from '@material-ui/core/Button';
import { Link } from '@material-ui/core';
import GridItem from '../Grid/GridItem';
const useStyles = makeStyles(theme => ({
  root: {},
  media: {
    height: 0,
    paddingTop: '56.25%', // 16:9
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  avatar: {
    backgroundColor: red[500],
  },
}));

export default function Playlist(props) {
  console.log('🚀 ~ file: index.jsx ~ line 43 ~ Playlist ~ props', props);
  const classes = useStyles();
  const [expanded, setExpanded] = React.useState(false);

  const handleExpandClick = () => {
    setExpanded(!expanded);
  };

  return (
    <GridItem xs={12} sm={6} md={3}>
      <Card>
        <CardMedia
          className={classes.media}
          image={props.playlist.icon}
          title="Paella dish"
        />
        <CardContent>
          <Typography
            gutterBottom
            variant="h5"
            component="h2"
            style={{ marginLeft: 0 }}
          >
            {props.playlist.name}
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
            There are {props.playlist.productDetails.length} places for sale
          </Typography>
        </CardContent>
        <CardActions disableSpacing>
          <IconButton
            className={clsx(classes.expand, {
              [classes.expandOpen]: expanded,
            })}
            onClick={handleExpandClick}
            aria-expanded={expanded}
            aria-label="show more"
          >
            <ExpandMoreIcon />
          </IconButton>
        </CardActions>
        <Collapse in={expanded} timeout="auto" unmountOnExit>
          {props.playlist.productDetails.map((item, i) => (
            <Card className={classes.root}>
              <CardHeader
                avatar={
                  <Avatar aria-label="recipe" className={classes.avatar}>
                    R
                  </Avatar>
                }
                action={
                  <IconButton aria-label="settings">
                    <MoreVertIcon />
                  </IconButton>
                }
                title={item.supplier}
                subheader={`Giá bán ${item.price}`}
              />
              {/* <CardActionArea>
                 <CardMedia
                   component="img"
                   alt="Contemplative Reptile"
                   height="140"
                   image="/static/images/cards/contemplative-reptile.jpg"
                   title="Contemplative Reptile"
                 />
                 <CardContent>
                   <Typography gutterBottom variant="h5" component="h2">
                     Lizard
                   </Typography>
                   <Typography variant="body2" color="textSecondary" component="p">
                     Lizards are a widespread group of squamate reptiles, with over
                     6,000 species, ranging across all continents except Antarctica
                   </Typography>
                 </CardContent>
               </CardActionArea> */}
              <CardActions disableSpacing>
                <IconButton aria-label="share">
                  <Link href={item.productPath}>
                    <ShareIcon />
                  </Link>
                </IconButton>
              </CardActions>
            </Card>
          ))}
        </Collapse>
      </Card>
    </GridItem>
  );
}
