/* eslint-disable react/prop-types */
import React, { useState } from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import CssBaseline from '@material-ui/core/CssBaseline';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import { Button, Hidden, InputAdornment, TextField } from '@material-ui/core';
import SearchIcon from '@material-ui/icons/Search';
import { NavLink, useHistory } from 'react-router-dom';
import Footer from '../Footer';
import logo from '../../images/logo.png';
import HeaderLinks from './HeaderLink';

const drawerWidth = 240;

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
  },
  appBar: {
    backgroundColor: '#0F171E',
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    backgroundColor: '#0F171E',
    zIndex: theme.zIndex.drawer + 1,
    [theme.breakpoints.up('sm')]: {
      transition: theme.transitions.create(['width', 'margin'], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen,
      }),
    },
  },
  menuButton: {
    [theme.breakpoints.down('sm')]: {
      display: 'none',
    },
  },
  hide: {
    display: 'none',
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
    whiteSpace: 'nowrap',
  },
  drawerOpen: {
    width: drawerWidth,
    backgroundColor: '#0F171E',
    borderRight: '0px !important',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerClose: {
    backgroundColor: '#0F171E',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    borderRight: '0px !important',
    overflowX: 'hidden',
    width: theme.spacing(7),
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing(9),
    },
  },
  toolbar: {
    backgroundColor: '#0F171E',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
  logo: {
    marginRight: 10,
    [theme.breakpoints.down('sm')]: {
      width: 80,
    },
  },
  search: {
    [theme.breakpoints.down('sm')]: {
      display: 'none',
    },
  },
  navLink: {
    color: 'inherit',
    position: 'relative',
    fontWeight: '400',
    fontSize: '12px',
    textTransform: 'uppercase',
    borderRadius: '3px',
    lineHeight: '20px',
    textDecoration: 'none',
    margin: '0 auto',
    display: 'inline-flex',
    '&:hover,&:focus': {
      color: 'inherit',
      background: 'rgba(200, 200, 200, 0.2)',
    },
    [theme.breakpoints.up('sm')]: {
      display: 'none',
      padding: '0.5rem',
    },
  },
  menuButtonMobile: {
    [theme.breakpoints.up('sm')]: {
      display: 'none',
    },
  },
}));

function Header(props) {
  const classes = useStyles();
  const history = useHistory({ forceRefresh: true });
  const [query, setQuery] = useState('');
  const handleDrawerToggle = () => {
    props.handleCollapseMobile(!props.mobileCollapse);
  };
  const handleDrawerOpen = () => {
    if (
      props.location.pathname === '/watch' ||
      props.location.pathname === '/profile'
    ) {
      props.handleCollapseOverlay(!props.collapseOverlay);
    } else {
      props.handleCollapse(!props.collapse);
    }
  };
  const onKeyDown = e => {
    if (e.keyCode === 13) {
      history.push(`/?query=${query}`);
    }
  };
  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar
        position="fixed"
        className={clsx(classes.appBar, {
          [classes.appBarShift]: !props.collapse,
        })}
      >
        <Toolbar>
          <NavLink to="/">
            <img className={classes.logo} src={logo} alt="logo" />
          </NavLink>

          <TextField
            className={classes.search}
            id="outlined-size-small"
            placeholder="Search and comparison easy product"
            variant="outlined"
            size="small"
            onChange={e => setQuery(e.target.value)}
            fullWidth
            onKeyDown={onKeyDown}
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <SearchIcon />
                </InputAdornment>
              ),
            }}
          />

          <Button
            target="_blank"
            className={classes.navLink}
            // color="transparent"
          >
            <SearchIcon style={{ width: 24, height: 24 }} />
          </Button>
          <HeaderLinks />
        </Toolbar>
      </AppBar>
      <main className={classes.content}>
        <div className={classes.toolbar} />
        <props.component {...props} />
        <Footer />
      </main>
    </div>
  );
}

export default Header;
