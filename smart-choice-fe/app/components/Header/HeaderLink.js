import React from 'react';
// react components for routing our app without refresh
import { NavLink } from 'react-router-dom';
// @material-ui/core components
import { makeStyles } from '@material-ui/core/styles';
import { yellow } from '@material-ui/core/colors';
// @material-ui/icons

import VideocamIcon from '@material-ui/icons/Videocam';
import FavoriteIcon from '@material-ui/icons/Favorite';
import NotificationsIcon from '@material-ui/icons/Notifications';
import PersonIcon from '@material-ui/icons/Person';
// core components

import { Hidden, MenuItem, MenuList, Popper, Tooltip } from '@material-ui/core';
import Button from '../CustomButtons/Button';

import styles from '../../assets/jss/material-kit-react/components/headerLinksStyle';
// svg icon
import FlagUSA from '../../images/united-states-of-america.svg';
import FlagVN from '../../images/vietnam.svg';
import { LANGUAGE } from '../../utils/constants';

const useStyles = makeStyles(styles);

// eslint-disable-next-line react/prop-types
function HeaderLinks() {
  const classes = useStyles();
  const [language, setLanguage] = React.useState(LANGUAGE.VN);
  const changeLanguage = () => {
    if (language === LANGUAGE.VN) {
      setLanguage(LANGUAGE.EN);
      return;
    }
    setLanguage(LANGUAGE.VN);
  };
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = event => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };
  return (
    <div>
      <Hidden smDown implementation="css">
        <div style={{ display: 'flex' }}>
          <NavLink to="/favorite" style={{ textDecoration: 'none' }}>
            <Button color="transparent" className={classes.navLink}>
              <FavoriteIcon
                fontSize="large"
                className={classes.socialIcons}
                style={{ width: 32, height: 32, color: 'white' }}
              />
            </Button>
          </NavLink>
          <Button
            color="transparent"
            target="_blank"
            className={classes.navLink}
          >
            <NotificationsIcon
              fontSize="large"
              className={classes.socialIcons}
              style={{ color: yellow[500], width: 32, height: 32 }}
            />
          </Button>

          <Button
            onMouseEnter={handleClick}
            color="transparent"
            className={classes.navLink}
          >
            <PersonIcon
              fontSize="large"
              className={classes.socialIcons}
              style={{ width: 32, height: 32 }}
            />
          </Button>
          <Popper
            open={Boolean(anchorEl)}
            anchorEl={anchorEl}
            onMouseLeave={handleClose}
            placement="bottom-end"
          >
            <MenuList
              id="simple-menu"
              style={{ backgroundColor: 'rgba(15, 23, 30, 0.9)' }}
            >
              <MenuItem component={NavLink} to="/profile">
                Kênh của bạn
              </MenuItem>
              <MenuItem>Logout</MenuItem>
            </MenuList>
          </Popper>

          <Tooltip
            title={
              language === LANGUAGE.VN ? 'Thay đổi ngôn ngữ' : 'Change Language'
            }
          >
            <Button
              color="transparent"
              target="_blank"
              onClick={changeLanguage}
              className={classes.navLink}
            >
              {language === LANGUAGE.VN ? (
                <img className={classes.svgIcon} src={FlagVN} alt="FlagVN" />
              ) : (
                <img className={classes.svgIcon} src={FlagUSA} alt="FlagUSA" />
              )}
            </Button>
          </Tooltip>
        </div>
      </Hidden>
      <Hidden mdUp implementation="js">
        <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
          <NavLink to="/favorite" style={{ textDecoration: 'none' }}>
            <Button
              target="_blank"
              className={classes.navLink}
              color="transparent"
            >
              <FavoriteIcon
                fontSize="large"
                className={classes.socialIcons}
                style={{ width: 24, height: 24, color: 'white' }}
              />
            </Button>
          </NavLink>
          <Button
            target="_blank"
            className={classes.navLink}
            color="transparent"
          >
            <NotificationsIcon
              fontSize="large"
              className={classes.socialIcons}
              style={{ color: yellow[500], width: 24, height: 24 }}
            />
          </Button>
          <Button
            onMouseEnter={handleClick}
            className={classes.navLink}
            color="transparent"
          >
            <PersonIcon
              fontSize="large"
              className={classes.socialIcons}
              style={{ width: 24, height: 24 }}
            />
          </Button>
          <Popper
            open={Boolean(anchorEl)}
            anchorEl={anchorEl}
            onMouseLeave={handleClose}
            placement="bottom-end"
          >
            <MenuList
              id="simple-menu"
              style={{ backgroundColor: 'rgba(15, 23, 30, 0.9)' }}
            >
              <MenuItem component={NavLink} to="/profile">
                Kênh của bạn
              </MenuItem>
              <MenuItem>Logout</MenuItem>
            </MenuList>
          </Popper>
          <Tooltip
            title={
              language === LANGUAGE.VN ? 'Thay đổi ngôn ngữ' : 'Change Language'
            }
          >
            <Button
              color="transparent"
              target="_blank"
              onClick={changeLanguage}
              className={classes.navLink}
            >
              {language === LANGUAGE.VN ? (
                <img className={classes.svgIcon} src={FlagVN} alt="FlagVN" />
              ) : (
                <img className={classes.svgIcon} src={FlagUSA} alt="FlagUSA" />
              )}
            </Button>
          </Tooltip>
        </div>
      </Hidden>
    </div>
  );
}

export default HeaderLinks;
