package com.smartchoice.productproceser.service.category.impl;

import com.smartchoice.common.dto.ProductRequest;
import com.smartchoice.productproceser.service.product.ProductService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import com.smartchoice.common.dto.CategoryResponse;
import com.smartchoice.common.model.rabbitmq.ExchangeName;
import com.smartchoice.common.model.rabbitmq.QueueName;
import com.smartchoice.productproceser.models.Category;
import com.smartchoice.productproceser.models.SupplyCategory;
import com.smartchoice.productproceser.service.supplycategory.SupplyCategoryService;
import org.springframework.amqp.AmqpRejectAndDontRequeueException;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.smartchoice.productproceser.service.category.CategoryService;

import java.util.List;

@Component
public class CategoryResponseProcessor {
    private static final Logger log = LogManager.getLogger(CategoryResponseProcessor.class);

    @Autowired
    private CategoryService categoryService;
    @Autowired
    private AmqpTemplate amqpTemplate;

    @Autowired
    private ProductService productService;

    @Autowired
    private SupplyCategoryService supplyCategoryService;

    @Value("${spring.rabbitmq.self-config.max-attempts}")
    private Long maxAttempts;
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void process(CategoryResponse categoryResponse) {
        try {
            log.info("Processing  categoryResponse {}", categoryResponse);
            categoryResponse.increaseAttempts();
            Long requestId = categoryResponse.getRequestId();
            Category category = categoryService.findById(requestId);
            if (category == null) {
                log.warn("Category processor not requestId{}", requestId);
                return;
            }

            SupplyCategory supplyCategory = new SupplyCategory();
            supplyCategory.setCategory(category);
            supplyCategory.setName(categoryResponse.getName());
            supplyCategory.setSupplier(categoryResponse.getSupplier());
            supplyCategory.setExternalCategoryId(categoryResponse.getId());

            supplyCategoryService.save(supplyCategory);
            log.info("Processed the categoryResponse {} and got a supplyCategory {}", categoryResponse, supplyCategory);

            List<String> productPrimaryKeySearch = category.getProductPrimaryKeySearch();
            productPrimaryKeySearch.forEach((search -> {
                ProductRequest productRequest = new ProductRequest(supplyCategory.getExternalCategoryId(), category.getId(), search);
                log.info("Starting notifying the productRequest{}", productRequest);
                productService.notifySupplier(productRequest);
            }));
        } catch (Exception e) {
            log.error("Exception the category response {}", categoryResponse, e);
            if (categoryResponse.getConsumerAttempts() < maxAttempts) {
                log.info("Sending categoryResponseto retry {}", categoryResponse);
                amqpTemplate.convertAndSend(ExchangeName.SC_RABBITMQ_DIRECT_EXCHANGE_NAME_CATEGORY_RESPONSE_RETRY,
                        QueueName.SC_RABBITMQ_QUEUE_NAME_CATEGORY_RESPONSE_RETRY, categoryResponse);
            } else {
                throw new AmqpRejectAndDontRequeueException("Exceeded maximum categoryResponse. " + categoryResponse , e);
            }
        }
    }
}
