package com.smartchoice.productproceser.service.category;


import com.smartchoice.productproceser.dto.CategoryInfo;
import com.smartchoice.productproceser.models.Category;

import java.util.List;


public interface CategoryService {

    List<CategoryInfo> findAll();

    CategoryInfo save(CategoryInfo category);

    List<CategoryInfo> save(List<CategoryInfo> categoryInfos);

    void delete(Long id);

    Category findById(Long id);

    void refresh(Long id);
}
