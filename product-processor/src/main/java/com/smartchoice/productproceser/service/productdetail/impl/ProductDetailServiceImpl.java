package com.smartchoice.productproceser.service.productdetail.impl;

import java.util.List;

import com.smartchoice.productproceser.models.ProductDetail;
import com.smartchoice.productproceser.repositories.productdetail.ProductDetailRepository;
import com.smartchoice.productproceser.service.productdetail.ProductDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.smartchoice.common.model.Supplier;


@Service
@Transactional
public class ProductDetailServiceImpl implements ProductDetailService {

    @Autowired
    private ProductDetailRepository productDetailRepository;

    @Override
    public List<ProductDetail> findAll()
    {
        return (List<ProductDetail>) productDetailRepository.findAll();
    }

    @Override
    public ProductDetail save(ProductDetail productDetail)
    {
       return productDetailRepository.save(productDetail);
    }

    @Override
    public void delete(Long id)
    {
       productDetailRepository.deleteById(id);
    }

    @Override
    public ProductDetail findById(Long id) {
        return productDetailRepository.findById(id).orElse(null);
    }

    @Override
    public ProductDetail find(Long externalId, Supplier supplier) {
        return productDetailRepository.find(externalId, supplier);
    }
}
