package com.smartchoice.productproceser.service.supplycategory;

import java.util.List;

import com.smartchoice.productproceser.models.SupplyCategory;

public interface SupplyCategoryService {

    List<SupplyCategory> findAll();

    SupplyCategory save(SupplyCategory supplyCategory);

    void delete(Long id);

    SupplyCategory findById(Long id);

    SupplyCategory findByExternalId(Long id);

}
