package com.smartchoice.productproceser.service.supplycategory.impl;

import java.util.List;

import com.smartchoice.productproceser.models.SupplyCategory;
import com.smartchoice.productproceser.repositories.supplycategory.SupplyCategoryRepository;
import com.smartchoice.productproceser.service.supplycategory.SupplyCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;



@Service
@Transactional
public class SupplyCategoryServiceImpl implements SupplyCategoryService {

    @Autowired
    private SupplyCategoryRepository supplyCategoryRepository;

    @Override
    public SupplyCategory save(SupplyCategory supplyCategory) {
        return supplyCategoryRepository.save(supplyCategory);
    }

    @Override
    public List<SupplyCategory> findAll() {
        return (List<SupplyCategory>) supplyCategoryRepository.findAll();
    }

    @Override
    public void delete(Long id) {
        supplyCategoryRepository.deleteById(id);
    }

    @Override
    public SupplyCategory findById(Long id) {
        return supplyCategoryRepository.findById(id).orElse(null);
    }

    @Override
    public SupplyCategory findByExternalId(Long id) {
        return supplyCategoryRepository.findByExternalId(id);
    }
}
