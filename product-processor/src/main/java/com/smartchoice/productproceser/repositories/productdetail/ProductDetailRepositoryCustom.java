package com.smartchoice.productproceser.repositories.productdetail;

import com.smartchoice.common.model.Supplier;
import com.smartchoice.productproceser.models.ProductDetail;

public interface ProductDetailRepositoryCustom {
    ProductDetail find(Long externalId, Supplier supplier);
}
