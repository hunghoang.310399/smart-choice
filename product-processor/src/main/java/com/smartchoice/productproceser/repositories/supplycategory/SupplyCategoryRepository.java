package com.smartchoice.productproceser.repositories.supplycategory;
import com.smartchoice.productproceser.models.SupplyCategory;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;



@Repository
public interface SupplyCategoryRepository extends CrudRepository<SupplyCategory, Long>, SupplyCategoryRepositoryCustom {
}
