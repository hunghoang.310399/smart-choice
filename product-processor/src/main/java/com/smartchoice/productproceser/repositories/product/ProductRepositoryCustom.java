package com.smartchoice.productproceser.repositories.product;

import java.util.List;

public interface ProductRepositoryCustom {
    Long findWithTrigramsAlgorithm(String productNameSearch, Double threshold);

    List<Long> findManyWithTrigramsAlgorithm(String fullSearchText, Double threshold);
}
