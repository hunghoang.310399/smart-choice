package com.smartchoice.productproceser.repositories.product;

import com.smartchoice.productproceser.models.Product;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface ProductRepository extends CrudRepository<Product, Long>, ProductRepositoryCustom {
}
