package com.smartchoice.productproceser.repositories.supplycategory;
import com.smartchoice.productproceser.models.SupplyCategory;

public interface SupplyCategoryRepositoryCustom {
    public SupplyCategory findByExternalId(Long id);
}
