package com.smartchoice.productproceser.repositories.productdetail;

import com.smartchoice.productproceser.models.ProductDetail;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductDetailRepository extends CrudRepository<ProductDetail, Long>, ProductDetailRepositoryCustom {
}
