package com.smartchoice.productproceser;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;


@SpringBootApplication
@EnableEurekaClient
public class ProductProceserApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProductProceserApplication.class, args);
    }

}
