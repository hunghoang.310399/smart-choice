package com.smartchoice.productproceser.config;
import com.smartchoice.productproceser.service.category.CategoryResponseConsumer;
import com.smartchoice.productproceser.service.product.ProductResponseConsumer;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import com.smartchoice.common.model.rabbitmq.QueueName;
import com.smartchoice.common.util.RabbitMQMessageConverter;


@Component
public class RabbitQueueConfiguration {



    @Bean
    public MessageConverter jsonMessageConverter() {
        return new RabbitMQMessageConverter();
    }

    @Bean
    MessageListenerAdapter categoryResponseListenerAdapter(CategoryResponseConsumer categoryResponseConsumer) {
        MessageListenerAdapter adapter = new MessageListenerAdapter(categoryResponseConsumer, "consume");
        adapter.setMessageConverter(jsonMessageConverter());
        return adapter;
    }

    @Bean
    SimpleMessageListenerContainer categoryResponseContainer(ConnectionFactory connectionFactory,
                                                             MessageListenerAdapter categoryResponseListenerAdapter) {
        SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
        container.setConnectionFactory(connectionFactory);
        container.setQueueNames(QueueName.SC_RABBITMQ_QUEUE_NAME_CATEGORY_RESPONSE_MAIN);
        container.setMessageListener(categoryResponseListenerAdapter);
        return container;
    }

    @Bean
    MessageListenerAdapter productResponseListenerAdapter(ProductResponseConsumer productResponseConsumer) {
        MessageListenerAdapter adapter = new MessageListenerAdapter(productResponseConsumer, "consume");
        adapter.setMessageConverter(jsonMessageConverter());
        return adapter;
    }

    @Bean
    SimpleMessageListenerContainer productResponseContainer(ConnectionFactory connectionFactory,
                                                            MessageListenerAdapter productResponseListenerAdapter) {
        SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
        container.setConnectionFactory(connectionFactory);
        container.setQueueNames(QueueName.SC_RABBITMQ_QUEUE_NAME_PRODUCT_RESPONSE_MAIN);
        container.setMessageListener(productResponseListenerAdapter);
        return container;
    }
}
